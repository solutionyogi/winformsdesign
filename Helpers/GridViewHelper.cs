﻿using System;
using DevExpress.XtraGrid.Views.Grid;
using Lux.BusinessIntelligence.Studio.Core;

namespace Lux.BusinessIntelligence.Studio.Helpers
{
    public static class GridViewHelper
    {
        public static ReadOnlyGridViewFormat<TInput> Create<TInput>(GridView gridView) where TInput : class
        {
            if (gridView == null)
                throw new ArgumentNullException("gridView");

            var result = new ReadOnlyGridViewFormat<TInput>
            {
                GridView = gridView
            };

            return result;
        }
    }
}