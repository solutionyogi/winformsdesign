﻿using System.ComponentModel;
using System.Diagnostics;

namespace Lux.BusinessIntelligence.Studio.Helpers
{
    public static class DesignTimeHelper
    {
        public static bool IsInDesignMode
        {
            get
            {
                var isInDesignMode = LicenseManager.UsageMode == LicenseUsageMode.Designtime || Debugger.IsAttached;

                if(isInDesignMode)
                    return true;

                using(var process = Process.GetCurrentProcess())
                {
                    return process.ProcessName.ToLowerInvariant().Contains("devenv");
                }
            }
        }
    }
}