﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;

namespace Lux.BusinessIntelligence.Studio.Helpers
{
    public static class MiscExtensionMethods
    {
        public static void SetPlacement(this Form windowForm, string placementXml)
        {
            if(windowForm == null)
                throw new ArgumentNullException("windowForm");

            if(string.IsNullOrWhiteSpace(placementXml))
                throw new ArgumentNullException("placementXml");

            WindowPlacementHelper.SetPlacement(windowForm.Handle, placementXml);
        }

        public static string GetPlacement(this Form windowForm)
        {
            if(windowForm == null)
                throw new ArgumentNullException("windowForm");

            return WindowPlacementHelper.GetPlacement(windowForm.Handle);
        }

        public static EditorButton FindEditorButtonByTag<T>(this RepositoryItemButtonEdit buttonEdit, T tagValue)
        {
            if(buttonEdit == null)
                throw new ArgumentNullException("buttonEdit");

            if(ReferenceEquals(tagValue, null))
                throw new ArgumentNullException("tagValue");

/*            foreach(var button in from button in buttonEdit.Buttons.Cast<EditorButton>()
                where !string.IsNullOrWhiteSpace(button.Tag.ToString())
                let buttonTagValue = DataHelper.ConvertTo<T>(button.Tag)
                where buttonTagValue.Equals(tagValue)
                select button)
            {
                return button;
            }*/

            throw new InvalidOperationException(string.Format("Could not find EditorButton with Tag {0}", tagValue));
        }
    }
}