using Lux.BusinessIntelligence.Studio.Core;

namespace Lux.BusinessIntelligence.Studio.Helpers
{
    public static class EventHelper
    {
        public static ViewModelEventDetail<TViewModel> Create<TViewModel>(TViewModel viewModel, ViewModelEvent @event)
        {
            var result = new ViewModelEventDetail<TViewModel>
                         {
                             ViewModel = viewModel,
                             Event = @event
                         };
            return result;
        }
    }
}