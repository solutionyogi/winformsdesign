﻿using System.Drawing;
using DevExpress.XtraGrid;

namespace Lux.BusinessIntelligence.Studio.Helpers
{
    public class GridConditionalFormatInfo
    {
        private Color _backColor = Color.Red;

        private Color _foreColor = Color.White;

        public Color BackColor
        {
            get { return _backColor; }
            set { _backColor = value; }
        }

        public Color ForeColor
        {
            get { return _foreColor; }
            set { _foreColor = value; }
        }

        public string Condition { get; set; }

        public StyleFormatCondition GetFormatCondition()
        {
            var condition = new StyleFormatCondition
                            {
                                Condition = FormatConditionEnum.Expression,
                                Expression = Condition
                            };
            condition.Appearance.BackColor = BackColor;
            condition.Appearance.ForeColor = ForeColor;
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.Options.UseForeColor = true;
            return condition;
        }
    }
}