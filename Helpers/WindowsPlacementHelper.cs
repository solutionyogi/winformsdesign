﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using NLog;

namespace Lux.BusinessIntelligence.Studio.Helpers
{
    // RECT structure required by WINDOWPLACEMENT structure
    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct Rect
    {
        public int Left;

        public int Top;

        public int Right;

        public int Bottom;

        public Rect(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
    }

    // POINT structure required by WINDOWPLACEMENT structure
    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct Point
    {
        public int X;

        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    // WINDOWPLACEMENT stores the position, size, and state of a window
    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct WindowPlacement
    {
        public int length;

        public int flags;

        public int showCmd;

        public Point minPosition;

        public Point maxPosition;

        public Rect normalPosition;
    }

    internal static class NativeMethods
    {
        [DllImport("user32.dll")]
        internal static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WindowPlacement lpwndpl);

        [DllImport("user32.dll")]
        internal static extern bool GetWindowPlacement(IntPtr hWnd, out WindowPlacement lpwndpl);
    }

    public static class WindowPlacementHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly Encoding Encoding = new UTF8Encoding();

        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(WindowPlacement));

        // ReSharper disable InconsistentNaming
        private const int SW_SHOWNORMAL = 1;

        private const int SW_SHOWMINIMIZED = 2;

        // ReSharper restore InconsistentNaming

        public static void SetPlacement(IntPtr windowHandle, string placementXml)
        {
            if(string.IsNullOrEmpty(placementXml))
                return;

            var xmlBytes = Encoding.GetBytes(placementXml);

            try
            {
                WindowPlacement placement;
                using(var memoryStream = new MemoryStream(xmlBytes))
                {
                    placement = (WindowPlacement) Serializer.Deserialize(memoryStream);
                }

                placement.length = Marshal.SizeOf(typeof(WindowPlacement));
                placement.flags = 0;
                placement.showCmd = (placement.showCmd == SW_SHOWMINIMIZED ? SW_SHOWNORMAL : placement.showCmd);
                NativeMethods.SetWindowPlacement(windowHandle, ref placement);
            }
            catch(InvalidOperationException e)
            {
                Logger.Error("SetPlacement failed.", e);
            }
        }

        public static string GetPlacement(IntPtr windowHandle)
        {
            WindowPlacement placement;
            NativeMethods.GetWindowPlacement(windowHandle, out placement);

            using(var memoryStream = new MemoryStream())
            {
                using(var xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8))
                {
                    Serializer.Serialize(xmlTextWriter, placement);
                    var xmlBytes = memoryStream.ToArray();
                    return Encoding.GetString(xmlBytes);
                }
            }
        }
    }
}