﻿using System;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public partial class TestForm1 : XtraForm
    {
        public TestForm1()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Initialize();
        }

        private void Initialize()
        {
            LabelLayoutGroup.Expanded = true;
            PropertyGridLayoutGroup.Expanded = !LabelLayoutGroup.Expanded;
        }

        private void ToggleVisibilityButtonClick(object sender, EventArgs e)
        {
            LabelLayoutGroup.Expanded = !LabelLayoutGroup.Expanded;
            PropertyGridLayoutGroup.Expanded = !LabelLayoutGroup.Expanded;
        }
    }
}