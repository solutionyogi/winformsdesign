﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Lux.BusinessIntelligence.Studio.Core;
using Lux.BusinessIntelligence.Studio.DTO;
using Lux.BusinessIntelligence.Studio.Helpers;
using GridColumnFormatInfo = Lux.BusinessIntelligence.Studio.Core.GridColumnFormatInfo;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public partial class ReadOnlyGridView : ViewBase
    {
        private GridColumn OperationActionColumn { get; set; }

        private readonly ReadOnlyGridViewFormat<OperationInfo> _mainViewFormat = new ReadOnlyGridViewFormat<OperationInfo>();

        private readonly RepositoryItemButtonEdit RestartCancelButtonEdit = new RepositoryItemButtonEdit();

        public new ReadOnlyGridViewModeld ViewModel
        {
            get { return GetViewModel<ReadOnlyGridViewModeld>(); }
        }

        public ReadOnlyGridView()
        {
            InitializeComponent();
        }

        public override void Initialize()
        {
            SetupOperationMasterView();
            ViewModel.PropertyChanged += (sender, args) =>
                                         {
                                             if(args.PropertyName == "IsBusy" && !ViewModel.IsBusy)
                                                 MainGridView.BestFitColumns();
                                         };
            MainGridControl.DataSource = ViewModel.DataSource;
        }

        private void SetupOperationMasterView()
        {
            GridViewColumnFormat();
            _mainViewFormat.ApplyFormat();


            OperationActionColumn = new GridColumn();
            OperationActionColumn.Caption = "Action";
            OperationActionColumn.ColumnEdit = RestartCancelButtonEdit;
            OperationActionColumn.MaxWidth = 70;
            OperationActionColumn.MinWidth = 70;
            OperationActionColumn.Name = "OperationActionColumn";
            OperationActionColumn.ShowButtonMode = ShowButtonModeEnum.ShowAlways;
            OperationActionColumn.Visible = true;
            OperationActionColumn.VisibleIndex = 5;
            OperationActionColumn.Width = 83;
            OperationActionColumn.OptionsFilter.AllowAutoFilter = false;

            MainGridControl.RepositoryItems.AddRange(new RepositoryItem[] {RestartCancelButtonEdit});

            MainGridView.Columns.Add(OperationActionColumn);
            MainGridView.CustomRowCellEdit += MainViewCustomRowCellEdit;
        }

        private void GridViewColumnFormat()
        {
            _mainViewFormat.AddColumn(x => x.OperationInfoId);
            _mainViewFormat.AddColumn(x => x.Description);
            _mainViewFormat.AddColumn(x => x.Description, new GridColumnFormatInfo
                                                          {
                                                              FilterPopupMode = FilterPopupMode.DateSmart
                                                          });
            _mainViewFormat.AddColumn(x => x.ModifiedBy);
            _mainViewFormat.AddColumn(x => x.ModifiedOn);

            _mainViewFormat.AddCalculatedColumn("[OperationInfoId] * 100", typeof(decimal), "OperationInfoIdMultiplied");
            _mainViewFormat.AddBackgroundColorFormatting("[OperationInfoId] > 3", Color.Red);
        }

        private void RefreshButtonClicked(object sender, EventArgs e)
        {
            ViewModel.RefreshData();
        }

        private void RestartCancelButtonEditClicked(object sender, ButtonPressedEventArgs e)
        {
            MessageBox.Show("You tried to restart or cancel!");
        }

        private void MainViewCustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if(e.Column != OperationActionColumn)
                return;

            var buttonEdit = RestartCancelButtonEdit;
            //buttonEdit.ButtonClick = RestartCancelButtonEditClicked;
            // this executes first!
            //var restartButton = buttonEdit.FindEditorButtonByTag(ButtonType.RestartButton);
            //var cancelButton = buttonEdit.FindEditorButtonByTag(ButtonType.CancelButton);

            //restartButton.Visible = true;
            //cancelButton.Visible = false;
        }
    }

    public enum LuxColor
    {
        Red
    }
}