﻿namespace Lux.BusinessIntelligence.Studio.Views
{
    partial class HierarchicalReportView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.MainLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.MainReportGrid = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MainReportGridLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).BeginInit();
            this.MainLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainReportGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainReportGridLayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // MainLayoutControl
            // 
            this.MainLayoutControl.Controls.Add(this.MainReportGrid);
            this.MainLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayoutControl.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControl.Name = "MainLayoutControl";
            this.MainLayoutControl.Root = this.MainLayoutControlGroup;
            this.MainLayoutControl.Size = new System.Drawing.Size(860, 595);
            this.MainLayoutControl.TabIndex = 0;
            this.MainLayoutControl.Text = "layoutControl1";
            // 
            // MainLayoutControlGroup
            // 
            this.MainLayoutControlGroup.CustomizationFormText = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.MainLayoutControlGroup.GroupBordersVisible = false;
            this.MainLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.MainReportGridLayoutItem});
            this.MainLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControlGroup.Name = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.Size = new System.Drawing.Size(860, 595);
            this.MainLayoutControlGroup.Text = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.TextVisible = false;
            // 
            // MainReportGrid
            // 
            this.MainReportGrid.Location = new System.Drawing.Point(12, 12);
            this.MainReportGrid.MainView = this.gridView1;
            this.MainReportGrid.Name = "MainReportGrid";
            this.MainReportGrid.Size = new System.Drawing.Size(836, 571);
            this.MainReportGrid.TabIndex = 4;
            this.MainReportGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.MainReportGrid;
            this.gridView1.Name = "gridView1";
            // 
            // MainReportGridLayoutItem
            // 
            this.MainReportGridLayoutItem.Control = this.MainReportGrid;
            this.MainReportGridLayoutItem.CustomizationFormText = "MainReportGridLayoutItem";
            this.MainReportGridLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.MainReportGridLayoutItem.Name = "MainReportGridLayoutItem";
            this.MainReportGridLayoutItem.Size = new System.Drawing.Size(840, 575);
            this.MainReportGridLayoutItem.Text = "MainReportGridLayoutItem";
            this.MainReportGridLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.MainReportGridLayoutItem.TextToControlDistance = 0;
            this.MainReportGridLayoutItem.TextVisible = false;
            // 
            // HierarchicalReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 595);
            this.Controls.Add(this.MainLayoutControl);
            this.Name = "HierarchicalReportViewer";
            this.Text = "HierarchicalReportViewer";
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).EndInit();
            this.MainLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainReportGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainReportGridLayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl MainLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup MainLayoutControlGroup;
        private DevExpress.XtraGrid.GridControl MainReportGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem MainReportGridLayoutItem;
    }
}