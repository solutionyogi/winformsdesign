using System.Data;
using System.Windows.Forms;
using Lux.BusinessIntelligence.Studio.Core;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public class HierarchicalReportViewModel : ViewModelBase
    {
        private readonly BindingSource _reportDataSource = new BindingSource();

        public override string Title
        {
            get { return "Hierarchical Report Viewer"; }
        }

        private DataSet ReportData { get; set; }

        public BindingSource ReportDataSource
        {
            get { return _reportDataSource; }
        }

        public override void Initialize()
        {
            ReportData = HierarchyDataSetGenerator.Generate();

            _reportDataSource.DataSource = ReportData.Tables[0];
        }
    }
}