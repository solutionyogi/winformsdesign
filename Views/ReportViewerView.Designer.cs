﻿namespace Lux.BusinessIntelligence.Studio.Views
{
    partial class ReportViewerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.SelectedReportLookupEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.ReportGrid = new DevExpress.XtraGrid.GridControl();
            this.ReportGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.RunReportButton = new DevExpress.XtraEditors.SimpleButton();
            this.SelectedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.MainLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.SelectedDateEditLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ReportGridLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RunReportButtonLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.SelectedReportLookupEditLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).BeginInit();
            this.MainLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedReportLookupEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedDateEditLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportGridLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RunReportButtonLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedReportLookupEditLayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // MainLayoutControl
            // 
            this.MainLayoutControl.Controls.Add(this.SelectedReportLookupEdit);
            this.MainLayoutControl.Controls.Add(this.ReportGrid);
            this.MainLayoutControl.Controls.Add(this.RunReportButton);
            this.MainLayoutControl.Controls.Add(this.SelectedDateEdit);
            this.MainLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayoutControl.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControl.Name = "MainLayoutControl";
            this.MainLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(477, 224, 250, 350);
            this.MainLayoutControl.Root = this.MainLayoutControlGroup;
            this.MainLayoutControl.Size = new System.Drawing.Size(673, 425);
            this.MainLayoutControl.TabIndex = 0;
            this.MainLayoutControl.Text = "layoutControl1";
            // 
            // SelectedReportLookupEdit
            // 
            this.SelectedReportLookupEdit.Location = new System.Drawing.Point(48, 36);
            this.SelectedReportLookupEdit.Name = "SelectedReportLookupEdit";
            this.SelectedReportLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelectedReportLookupEdit.Size = new System.Drawing.Size(613, 20);
            this.SelectedReportLookupEdit.StyleController = this.MainLayoutControl;
            this.SelectedReportLookupEdit.TabIndex = 7;
            // 
            // ReportGrid
            // 
            this.ReportGrid.Location = new System.Drawing.Point(12, 86);
            this.ReportGrid.MainView = this.ReportGridView;
            this.ReportGrid.Name = "ReportGrid";
            this.ReportGrid.Size = new System.Drawing.Size(649, 327);
            this.ReportGrid.TabIndex = 6;
            this.ReportGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ReportGridView});
            // 
            // ReportGridView
            // 
            this.ReportGridView.GridControl = this.ReportGrid;
            this.ReportGridView.Name = "ReportGridView";
            // 
            // RunReportButton
            // 
            this.RunReportButton.Location = new System.Drawing.Point(12, 60);
            this.RunReportButton.MaximumSize = new System.Drawing.Size(125, 0);
            this.RunReportButton.Name = "RunReportButton";
            this.RunReportButton.Size = new System.Drawing.Size(125, 22);
            this.RunReportButton.StyleController = this.MainLayoutControl;
            this.RunReportButton.TabIndex = 5;
            this.RunReportButton.Text = "Run Report";
            this.RunReportButton.Click += new System.EventHandler(this.RunReportButtonClick);
            // 
            // SelectedDateEdit
            // 
            this.SelectedDateEdit.EditValue = null;
            this.SelectedDateEdit.Location = new System.Drawing.Point(48, 12);
            this.SelectedDateEdit.MaximumSize = new System.Drawing.Size(200, 0);
            this.SelectedDateEdit.Name = "SelectedDateEdit";
            this.SelectedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelectedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelectedDateEdit.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.SelectedDateEdit.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.SelectedDateEdit.Size = new System.Drawing.Size(200, 20);
            this.SelectedDateEdit.StyleController = this.MainLayoutControl;
            this.SelectedDateEdit.TabIndex = 4;
            // 
            // MainLayoutControlGroup
            // 
            this.MainLayoutControlGroup.CustomizationFormText = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.MainLayoutControlGroup.GroupBordersVisible = false;
            this.MainLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.SelectedDateEditLayoutItem,
            this.ReportGridLayoutItem,
            this.RunReportButtonLayoutItem,
            this.SelectedReportLookupEditLayoutItem});
            this.MainLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControlGroup.Name = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.Size = new System.Drawing.Size(673, 425);
            this.MainLayoutControlGroup.Text = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.TextVisible = false;
            // 
            // SelectedDateEditLayoutItem
            // 
            this.SelectedDateEditLayoutItem.Control = this.SelectedDateEdit;
            this.SelectedDateEditLayoutItem.CustomizationFormText = "Date";
            this.SelectedDateEditLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.SelectedDateEditLayoutItem.Name = "SelectedDateEditLayoutItem";
            this.SelectedDateEditLayoutItem.Size = new System.Drawing.Size(653, 24);
            this.SelectedDateEditLayoutItem.Text = "Date";
            this.SelectedDateEditLayoutItem.TextSize = new System.Drawing.Size(33, 13);
            // 
            // ReportGridLayoutItem
            // 
            this.ReportGridLayoutItem.Control = this.ReportGrid;
            this.ReportGridLayoutItem.CustomizationFormText = "ReportGridLayoutItem";
            this.ReportGridLayoutItem.Location = new System.Drawing.Point(0, 74);
            this.ReportGridLayoutItem.Name = "ReportGridLayoutItem";
            this.ReportGridLayoutItem.Size = new System.Drawing.Size(653, 331);
            this.ReportGridLayoutItem.Text = "ReportGridLayoutItem";
            this.ReportGridLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.ReportGridLayoutItem.TextToControlDistance = 0;
            this.ReportGridLayoutItem.TextVisible = false;
            // 
            // RunReportButtonLayoutItem
            // 
            this.RunReportButtonLayoutItem.Control = this.RunReportButton;
            this.RunReportButtonLayoutItem.CustomizationFormText = "RunReportButtonLayoutItem";
            this.RunReportButtonLayoutItem.Location = new System.Drawing.Point(0, 48);
            this.RunReportButtonLayoutItem.Name = "RunReportButtonLayoutItem";
            this.RunReportButtonLayoutItem.Size = new System.Drawing.Size(653, 26);
            this.RunReportButtonLayoutItem.Text = "RunReportButtonLayoutItem";
            this.RunReportButtonLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.RunReportButtonLayoutItem.TextToControlDistance = 0;
            this.RunReportButtonLayoutItem.TextVisible = false;
            // 
            // SelectedReportLookupEditLayoutItem
            // 
            this.SelectedReportLookupEditLayoutItem.Control = this.SelectedReportLookupEdit;
            this.SelectedReportLookupEditLayoutItem.CustomizationFormText = "Report";
            this.SelectedReportLookupEditLayoutItem.Location = new System.Drawing.Point(0, 24);
            this.SelectedReportLookupEditLayoutItem.Name = "SelectedReportLookupEditLayoutItem";
            this.SelectedReportLookupEditLayoutItem.Size = new System.Drawing.Size(653, 24);
            this.SelectedReportLookupEditLayoutItem.Text = "Report";
            this.SelectedReportLookupEditLayoutItem.TextSize = new System.Drawing.Size(33, 13);
            // 
            // ReportViewerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 425);
            this.Controls.Add(this.MainLayoutControl);
            this.Name = "ReportViewerView";
            this.Text = "ReportView";
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).EndInit();
            this.MainLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SelectedReportLookupEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedDateEditLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportGridLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RunReportButtonLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedReportLookupEditLayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl MainLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup MainLayoutControlGroup;
        private DevExpress.XtraEditors.DateEdit SelectedDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem SelectedDateEditLayoutItem;
        private DevExpress.XtraEditors.SimpleButton RunReportButton;
        private DevExpress.XtraLayout.LayoutControlItem RunReportButtonLayoutItem;
        private DevExpress.XtraGrid.GridControl ReportGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView ReportGridView;
        private DevExpress.XtraLayout.LayoutControlItem ReportGridLayoutItem;
        private DevExpress.XtraEditors.LookUpEdit SelectedReportLookupEdit;
        private DevExpress.XtraLayout.LayoutControlItem SelectedReportLookupEditLayoutItem;
    }
}