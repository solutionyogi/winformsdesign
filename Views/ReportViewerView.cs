﻿using System;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using Lux.BusinessIntelligence.Studio.Core;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public partial class ReportViewerView : ViewBase
    {
        public new ReportViewerViewModel ViewModel
        {
            get { return GetViewModel<ReportViewerViewModel>(); }
        }

        public ReportViewerView()
        {
            InitializeComponent();
        }

        public override LayoutControl MainContainer
        {
            get { return MainLayoutControl; }
        }

        public override void Initialize()
        {
            SelectedDateEdit.AddDataBinding(x => x.DateTime, ViewModel, x => x.SelectedDate);

            SelectedReportLookupEdit.Properties.DataSource = ViewModel.ReportListSource;
            SelectedReportLookupEdit.Properties.DisplayMember = "ReportName";
            SelectedReportLookupEdit.AddDataBinding(x => x.EditValue, ViewModel, x => x.SelectedReport);

            ReportGrid.DataSource = ViewModel.ReportDataSource;

            ViewModel.Subscribe(ViewModelEvent.CalculationInProgress, CalculationInProgressMessage);
        }

        public void CalculationInProgressMessage()
        {
            MessageBox.Show("Running report..." + ViewModel.SelectedReport.ReportName);
        }

        private void RunReportButtonClick(object sender, EventArgs e)
        {
            ViewModel.RunReport();
        }
    }
}