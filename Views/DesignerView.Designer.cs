﻿using Lux.BusinessIntelligence.Studio.DTO;

namespace Lux.BusinessIntelligence.Studio.Views
{
    partial class DesignerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MainLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.MainGridControl = new DevExpress.XtraGrid.GridControl();
            this.operationInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOperationInfoId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MainLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.GridLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).BeginInit();
            this.MainLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLayoutControlItem)).BeginInit();
            this.SuspendLayout();
            // 
            // MainLayoutControl
            // 
            this.MainLayoutControl.Controls.Add(this.MainGridControl);
            this.MainLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayoutControl.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControl.Name = "MainLayoutControl";
            this.MainLayoutControl.Root = this.MainLayoutControlGroup;
            this.MainLayoutControl.Size = new System.Drawing.Size(777, 595);
            this.MainLayoutControl.TabIndex = 0;
            this.MainLayoutControl.Text = "layoutControl1";
            // 
            // MainGridControl
            // 
            this.MainGridControl.DataSource = this.operationInfoBindingSource;
            this.MainGridControl.Location = new System.Drawing.Point(12, 12);
            this.MainGridControl.MainView = this.MainGridView;
            this.MainGridControl.Name = "MainGridControl";
            this.MainGridControl.Size = new System.Drawing.Size(753, 571);
            this.MainGridControl.TabIndex = 4;
            this.MainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.MainGridView});
            // 
            // operationInfoBindingSource
            // 
            this.operationInfoBindingSource.DataSource = typeof(Lux.BusinessIntelligence.Studio.DTO.OperationInfo);
            // 
            // MainGridView
            // 
            this.MainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOperationInfoId,
            this.colDescription,
            this.colModifiedBy,
            this.colModifiedOn});
            this.MainGridView.GridControl = this.MainGridControl;
            this.MainGridView.Name = "MainGridView";
            this.MainGridView.OptionsBehavior.Editable = false;
            this.MainGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.MainGridView.OptionsSelection.MultiSelect = true;
            this.MainGridView.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button;
            this.MainGridView.OptionsView.ShowAutoFilterRow = true;
            this.MainGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colOperationInfoId
            // 
            this.colOperationInfoId.FieldName = "OperationInfoId";
            this.colOperationInfoId.Name = "colOperationInfoId";
            this.colOperationInfoId.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOperationInfoId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOperationInfoId.Visible = true;
            this.colOperationInfoId.VisibleIndex = 0;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            // 
            // colModifiedBy
            // 
            this.colModifiedBy.FieldName = "ModifiedBy";
            this.colModifiedBy.Name = "colModifiedBy";
            this.colModifiedBy.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colModifiedBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colModifiedBy.Visible = true;
            this.colModifiedBy.VisibleIndex = 2;
            // 
            // colModifiedOn
            // 
            this.colModifiedOn.FieldName = "ModifiedOn";
            this.colModifiedOn.Name = "colModifiedOn";
            this.colModifiedOn.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colModifiedOn.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateAlt;
            this.colModifiedOn.Visible = true;
            this.colModifiedOn.VisibleIndex = 3;
            // 
            // MainLayoutControlGroup
            // 
            this.MainLayoutControlGroup.CustomizationFormText = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.MainLayoutControlGroup.GroupBordersVisible = false;
            this.MainLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.GridLayoutControlItem});
            this.MainLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControlGroup.Name = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.Size = new System.Drawing.Size(777, 595);
            this.MainLayoutControlGroup.Text = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.TextVisible = false;
            // 
            // GridLayoutControlItem
            // 
            this.GridLayoutControlItem.Control = this.MainGridControl;
            this.GridLayoutControlItem.CustomizationFormText = "layoutControlItem1";
            this.GridLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.GridLayoutControlItem.Name = "GridLayoutControlItem";
            this.GridLayoutControlItem.Size = new System.Drawing.Size(757, 575);
            this.GridLayoutControlItem.Text = "GridLayoutControlItem";
            this.GridLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.GridLayoutControlItem.TextToControlDistance = 0;
            this.GridLayoutControlItem.TextVisible = false;
            // 
            // DesignerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 595);
            this.Controls.Add(this.MainLayoutControl);
            this.Name = "DesignerView";
            this.Text = "GridFormatView";
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).EndInit();
            this.MainLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLayoutControlItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl MainLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup MainLayoutControlGroup;
        private DevExpress.XtraGrid.GridControl MainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView MainGridView;
        private DevExpress.XtraLayout.LayoutControlItem GridLayoutControlItem;
        private System.Windows.Forms.BindingSource operationInfoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOperationInfoId;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedOn;
    }
}