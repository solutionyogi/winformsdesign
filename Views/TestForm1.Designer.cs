﻿namespace Lux.BusinessIntelligence.Studio.Views
{
    partial class TestForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.MessageLabel = new DevExpress.XtraEditors.LabelControl();
            this.ToggleVisibilityButton = new DevExpress.XtraEditors.SimpleButton();
            this.FieldPropertyGrid = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.LeftNavBar = new DevExpress.XtraNavBar.NavBarControl();
            this.DefaultNavBarGroup = new DevExpress.XtraNavBar.NavBarGroup();
            this.MainLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LeftNavBarLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ToggleVisibilityButtonLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ParentLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LabelLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.MessageLabelLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PropertyGridLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.FieldPropertyGridLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).BeginInit();
            this.MainLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FieldPropertyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftNavBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftNavBarLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToggleVisibilityButtonLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParentLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MessageLabelLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyGridLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldPropertyGridLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // MainLayoutControl
            // 
            this.MainLayoutControl.Controls.Add(this.MessageLabel);
            this.MainLayoutControl.Controls.Add(this.ToggleVisibilityButton);
            this.MainLayoutControl.Controls.Add(this.FieldPropertyGrid);
            this.MainLayoutControl.Controls.Add(this.LeftNavBar);
            this.MainLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayoutControl.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControl.Name = "MainLayoutControl";
            this.MainLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(282, 494, 608, 731);
            this.MainLayoutControl.Root = this.MainLayoutControlGroup;
            this.MainLayoutControl.Size = new System.Drawing.Size(768, 695);
            this.MainLayoutControl.TabIndex = 0;
            this.MainLayoutControl.Text = "layoutControl1";
            // 
            // MessageLabel
            // 
            this.MessageLabel.Location = new System.Drawing.Point(479, 364);
            this.MessageLabel.Name = "MessageLabel";
            this.MessageLabel.Size = new System.Drawing.Size(63, 13);
            this.MessageLabel.StyleController = this.MainLayoutControl;
            this.MessageLabel.TabIndex = 7;
            this.MessageLabel.Text = "labelControl1";
            // 
            // ToggleVisibilityButton
            // 
            this.ToggleVisibilityButton.AutoWidthInLayoutControl = true;
            this.ToggleVisibilityButton.Location = new System.Drawing.Point(12, 12);
            this.ToggleVisibilityButton.Name = "ToggleVisibilityButton";
            this.ToggleVisibilityButton.Size = new System.Drawing.Size(43, 22);
            this.ToggleVisibilityButton.StyleController = this.MainLayoutControl;
            this.ToggleVisibilityButton.TabIndex = 6;
            this.ToggleVisibilityButton.Text = "Toggle";
            this.ToggleVisibilityButton.Click += new System.EventHandler(this.ToggleVisibilityButtonClick);
            // 
            // FieldPropertyGrid
            // 
            this.FieldPropertyGrid.Location = new System.Drawing.Point(278, 672);
            this.FieldPropertyGrid.Name = "FieldPropertyGrid";
            this.FieldPropertyGrid.Size = new System.Drawing.Size(442, 479);
            this.FieldPropertyGrid.TabIndex = 5;
            // 
            // LeftNavBar
            // 
            this.LeftNavBar.ActiveGroup = this.DefaultNavBarGroup;
            this.LeftNavBar.ExplorerBarStretchLastGroup = true;
            this.LeftNavBar.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.DefaultNavBarGroup});
            this.LeftNavBar.Location = new System.Drawing.Point(12, 38);
            this.LeftNavBar.MaximumSize = new System.Drawing.Size(250, 0);
            this.LeftNavBar.Name = "LeftNavBar";
            this.LeftNavBar.OptionsNavPane.ExpandedWidth = 250;
            this.LeftNavBar.Size = new System.Drawing.Size(250, 645);
            this.LeftNavBar.TabIndex = 4;
            this.LeftNavBar.Text = "navBarControl1";
            // 
            // DefaultNavBarGroup
            // 
            this.DefaultNavBarGroup.Caption = "navBarGroup1";
            this.DefaultNavBarGroup.Expanded = true;
            this.DefaultNavBarGroup.GroupClientHeight = 615;
            this.DefaultNavBarGroup.Name = "DefaultNavBarGroup";
            // 
            // MainLayoutControlGroup
            // 
            this.MainLayoutControlGroup.CustomizationFormText = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.MainLayoutControlGroup.GroupBordersVisible = false;
            this.MainLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LeftNavBarLayoutItem,
            this.ParentLayoutControlGroup,
            this.ToggleVisibilityButtonLayoutItem});
            this.MainLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.MainLayoutControlGroup.Name = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.Size = new System.Drawing.Size(768, 695);
            this.MainLayoutControlGroup.Text = "MainLayoutControlGroup";
            this.MainLayoutControlGroup.TextVisible = false;
            // 
            // LeftNavBarLayoutItem
            // 
            this.LeftNavBarLayoutItem.Control = this.LeftNavBar;
            this.LeftNavBarLayoutItem.CustomizationFormText = "LeftNavBarLayoutItem";
            this.LeftNavBarLayoutItem.Location = new System.Drawing.Point(0, 26);
            this.LeftNavBarLayoutItem.Name = "LeftNavBarLayoutItem";
            this.LeftNavBarLayoutItem.Size = new System.Drawing.Size(254, 649);
            this.LeftNavBarLayoutItem.Text = "LeftNavBarLayoutItem";
            this.LeftNavBarLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.LeftNavBarLayoutItem.TextToControlDistance = 0;
            this.LeftNavBarLayoutItem.TextVisible = false;
            // 
            // ToggleVisibilityButtonLayoutItem
            // 
            this.ToggleVisibilityButtonLayoutItem.Control = this.ToggleVisibilityButton;
            this.ToggleVisibilityButtonLayoutItem.CustomizationFormText = "ToggleVisibilityButtonLayoutItem";
            this.ToggleVisibilityButtonLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.ToggleVisibilityButtonLayoutItem.Name = "ToggleVisibilityButtonLayoutItem";
            this.ToggleVisibilityButtonLayoutItem.Size = new System.Drawing.Size(748, 26);
            this.ToggleVisibilityButtonLayoutItem.Text = "ToggleVisibilityButtonLayoutItem";
            this.ToggleVisibilityButtonLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.ToggleVisibilityButtonLayoutItem.TextToControlDistance = 0;
            this.ToggleVisibilityButtonLayoutItem.TextVisible = false;
            // 
            // ParentLayoutControlGroup
            // 
            this.ParentLayoutControlGroup.CustomizationFormText = "ParentLayoutControlGroup";
            this.ParentLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LabelLayoutGroup,
            this.PropertyGridLayoutGroup});
            this.ParentLayoutControlGroup.Location = new System.Drawing.Point(254, 26);
            this.ParentLayoutControlGroup.Name = "ParentLayoutControlGroup";
            this.ParentLayoutControlGroup.Size = new System.Drawing.Size(494, 649);
            this.ParentLayoutControlGroup.Text = "ParentLayoutControlGroup";
            // 
            // LabelLayoutGroup
            // 
            this.LabelLayoutGroup.CustomizationFormText = "LabelLayoutGroup";
            this.LabelLayoutGroup.GroupBordersVisible = false;
            this.LabelLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.MessageLabelLayoutItem});
            this.LabelLayoutGroup.Location = new System.Drawing.Point(0, 0);
            this.LabelLayoutGroup.Name = "LabelLayoutGroup";
            this.LabelLayoutGroup.Size = new System.Drawing.Size(470, 603);
            this.LabelLayoutGroup.Text = "LabelLayoutGroup";
            // 
            // MessageLabelLayoutItem
            // 
            this.MessageLabelLayoutItem.Control = this.MessageLabel;
            this.MessageLabelLayoutItem.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.MessageLabelLayoutItem.CustomizationFormText = "MessageLabelLayoutItem";
            this.MessageLabelLayoutItem.Location = new System.Drawing.Point(0, 295);
            this.MessageLabelLayoutItem.Name = "MessageLabelLayoutItem";
            this.MessageLabelLayoutItem.Size = new System.Drawing.Size(470, 17);
            this.MessageLabelLayoutItem.Text = "MessageLabelLayoutItem";
            this.MessageLabelLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.MessageLabelLayoutItem.TextToControlDistance = 0;
            this.MessageLabelLayoutItem.TextVisible = false;
            // 
            // PropertyGridLayoutGroup
            // 
            this.PropertyGridLayoutGroup.CustomizationFormText = "PropertyGridLayoutGroup";
            this.PropertyGridLayoutGroup.Expanded = false;
            this.PropertyGridLayoutGroup.GroupBordersVisible = false;
            this.PropertyGridLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.FieldPropertyGridLayoutItem});
            this.PropertyGridLayoutGroup.Location = new System.Drawing.Point(0, 603);
            this.PropertyGridLayoutGroup.Name = "PropertyGridLayoutGroup";
            this.PropertyGridLayoutGroup.Size = new System.Drawing.Size(470, 3);
            this.PropertyGridLayoutGroup.Text = "PropertyGridLayoutGroup";
            // 
            // FieldPropertyGridLayoutItem
            // 
            this.FieldPropertyGridLayoutItem.Control = this.FieldPropertyGrid;
            this.FieldPropertyGridLayoutItem.CustomizationFormText = "FieldPropertyGridLayoutItem";
            this.FieldPropertyGridLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.FieldPropertyGridLayoutItem.Name = "FieldPropertyGridLayoutItem";
            this.FieldPropertyGridLayoutItem.Size = new System.Drawing.Size(446, 483);
            this.FieldPropertyGridLayoutItem.Text = "FieldPropertyGridLayoutItem";
            this.FieldPropertyGridLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.FieldPropertyGridLayoutItem.TextToControlDistance = 0;
            this.FieldPropertyGridLayoutItem.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(470, 295);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 312);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(470, 291);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // TestForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 695);
            this.Controls.Add(this.MainLayoutControl);
            this.Name = "TestForm1";
            this.Text = "TestForm1";
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).EndInit();
            this.MainLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FieldPropertyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftNavBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftNavBarLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToggleVisibilityButtonLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParentLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MessageLabelLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyGridLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldPropertyGridLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl MainLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup MainLayoutControlGroup;
        private DevExpress.XtraNavBar.NavBarControl LeftNavBar;
        private DevExpress.XtraNavBar.NavBarGroup DefaultNavBarGroup;
        private DevExpress.XtraLayout.LayoutControlItem LeftNavBarLayoutItem;
        private DevExpress.XtraVerticalGrid.PropertyGridControl FieldPropertyGrid;
        private DevExpress.XtraLayout.LayoutControlItem FieldPropertyGridLayoutItem;
        private DevExpress.XtraEditors.SimpleButton ToggleVisibilityButton;
        private DevExpress.XtraLayout.LayoutControlItem ToggleVisibilityButtonLayoutItem;
        private DevExpress.XtraEditors.LabelControl MessageLabel;
        private DevExpress.XtraLayout.LayoutControlGroup ParentLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem MessageLabelLayoutItem;
        private DevExpress.XtraLayout.LayoutControlGroup LabelLayoutGroup;
        private DevExpress.XtraLayout.LayoutControlGroup PropertyGridLayoutGroup;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}