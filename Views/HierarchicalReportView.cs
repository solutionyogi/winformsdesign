﻿using Lux.BusinessIntelligence.Studio.Core;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public partial class HierarchicalReportView : ViewBase
    {
        public HierarchicalReportView()
        {
            InitializeComponent();
        }

        public new HierarchicalReportViewModel ViewModel
        {
            get
            {
                return GetViewModel<HierarchicalReportViewModel>();
            }
        }

        public override void Initialize()
        {
            MainReportGrid.DataSource = ViewModel.ReportDataSource;
        }
    }
}