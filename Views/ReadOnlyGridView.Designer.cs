﻿namespace Lux.BusinessIntelligence.Studio.Views
{
    partial class ReadOnlyGridView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.MainGridControl = new DevExpress.XtraGrid.GridControl();
            this.MainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.RefreshSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // MainGridControl
            // 
            this.MainGridControl.Location = new System.Drawing.Point(13, 47);
            this.MainGridControl.MainView = this.MainGridView;
            this.MainGridControl.Name = "MainGridControl";
            this.MainGridControl.Size = new System.Drawing.Size(658, 388);
            this.MainGridControl.TabIndex = 0;
            this.MainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.MainGridView});
            // 
            // MainGridView
            // 
            this.MainGridView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.MainGridView.GridControl = this.MainGridControl;
            this.MainGridView.Name = "MainGridView";
            this.MainGridView.OptionsView.HeaderFilterButtonShowMode = DevExpress.XtraEditors.Controls.FilterButtonShowMode.Button;
            // 
            // RefreshSimpleButton
            // 
            this.RefreshSimpleButton.Location = new System.Drawing.Point(13, 13);
            this.RefreshSimpleButton.Name = "RefreshSimpleButton";
            this.RefreshSimpleButton.Size = new System.Drawing.Size(132, 23);
            this.RefreshSimpleButton.TabIndex = 1;
            this.RefreshSimpleButton.Text = "Refresh";
            this.RefreshSimpleButton.Click += new System.EventHandler(this.RefreshButtonClicked);
            // 
            // ReadOnlyGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 447);
            this.Controls.Add(this.RefreshSimpleButton);
            this.Controls.Add(this.MainGridControl);
            this.Name = "ReadOnlyGridView";
            this.Text = "GridView";
            ((System.ComponentModel.ISupportInitialize)(this.MainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl MainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView MainGridView;
        private DevExpress.XtraEditors.SimpleButton RefreshSimpleButton;
    }
}