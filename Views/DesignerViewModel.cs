﻿using Lux.BusinessIntelligence.Studio.Core;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public class DesignerViewModel : ViewModelBase
    {
        public override string Title
        {
            get { return "Grid Format View"; }
        }

        public override void Initialize()
        {
        }
    }
}