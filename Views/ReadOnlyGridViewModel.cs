﻿using System.Windows.Forms;
using Lux.BusinessIntelligence.Studio.Core;
using Lux.BusinessIntelligence.Studio.DTO;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public class ReadOnlyGridViewModeld : ViewModelBase
    {
        private readonly BindingSource _dataSource = new BindingSource();

        public BindingSource DataSource
        {
            get { return _dataSource; }
        }

        public override string Title
        {
            get { return "Grid Format View"; }
        }

        public override void Initialize()
        {
            IsBusy = true;
            DataSource.DataSource = TestDataGenerator.GetSampleData();
            IsBusy = false;
        }

        public void RefreshData()
        {
            IsBusy = true;
            DataSource.DataSource = TestDataGenerator.GetSampleData();
            IsBusy = false;
        }
    }
}