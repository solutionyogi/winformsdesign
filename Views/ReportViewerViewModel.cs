using System;
using System.Linq;
using System.Windows.Forms;
using Lux.BusinessIntelligence.Studio.Core;
using Lux.BusinessIntelligence.Studio.DTO;
using Lux.BusinessIntelligence.Studio.Helpers;

namespace Lux.BusinessIntelligence.Studio.Views
{
    public class ReportViewerViewModel : ViewModelBase
    {
        private readonly BindingSource _reportListSource = new BindingSource();

        private readonly BindingSource _reportDataSource = new BindingSource();

        private DateTime _selectedDate;

        private ReportInfo _selectedReport;

        public override string Title
        {
            get { return "Report Viewer"; }
        }

        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                if(value.Equals(_selectedDate))
                    return;
                _selectedDate = value;
                RaisePropertyChanged(() => SelectedDate);
            }
        }

        public ReportInfo SelectedReport
        {
            get { return _selectedReport; }
            set
            {
                if(Equals(value, _selectedReport))
                    return;
                _selectedReport = value;
                RaisePropertyChanged(() => SelectedReport);
            }
        }

        public BindingSource ReportListSource
        {
            get { return _reportListSource; }
        }

        public BindingSource ReportDataSource
        {
            get { return _reportDataSource; }
        }

        public void RunReport()
        {
            ViewManager.EventAggregator.Publish(EventHelper.Create(this, ViewModelEvent.CalculationInProgress));
            ReportDataSource.DataSource = TestDataGenerator.GetSampleData();
        }

        public override void Initialize()
        {
            var reportInfoList = TestDataGenerator.GetReportInfoList();

            ReportListSource.DataSource = reportInfoList;

            SelectedReport = reportInfoList.First();

            SelectedDate = new DateTime(2009, 01, 01);
        }

        public override string ToString()
        {
            return string.Format("SelectedDate: {0}", SelectedDate);
        }
    }
}