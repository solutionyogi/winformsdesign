﻿using System;
using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using Lux.BusinessIntelligence.Core.Helpers;

namespace Lux.BusinessIntelligence.Studio.Core
{
    public class GridColumnFormatInfo
    {
        private readonly GridColumn _gridColumn = new GridColumn();

        public string FieldName { get; set; }

        public string FinalFieldName
        {
            get
            {
                if(! string.IsNullOrWhiteSpace(FieldName))
                    return FieldName;

                if(IsCalculatedColumn)
                    return "SomeLogicToGenerateUniqueCalculatedColumn";

                throw new InvalidOperationException("You must specify FieldName or Expression.");
            }
        }

        private string FinalColumnName
        {
            get { return string.Format("{0}Column", FinalFieldName); }
        }

        public string Caption { get; set; }

        private string FinalCaption
        {
            get { return !string.IsNullOrWhiteSpace(Caption) ? Caption : FinalFieldName; }
        }

        public string FormatString { get; set; }

        private string FinalFormatString
        {
            get { return ! string.IsNullOrWhiteSpace(FormatString) ? FormatString : DefaultFormatString; }
        }

        public string UnboundExpression { get; set; }

        private string FinalUnboundExpression
        {
            get { return !string.IsNullOrWhiteSpace(UnboundExpression) ? UnboundExpression : _gridColumn.UnboundExpression; }
        }

        private UnboundColumnType FinalUnboundExpressionType
        {
            get
            {
                if(NormalizedFieldType == typeof(string))
                    return UnboundColumnType.String;
                if(NormalizedFieldType == typeof(decimal))
                    return UnboundColumnType.Decimal;
                if(NormalizedFieldType == typeof(int))
                    return UnboundColumnType.Integer;
                if(NormalizedFieldType == typeof(float))
                    return UnboundColumnType.Decimal;
                if(NormalizedFieldType == typeof(bool))
                    return UnboundColumnType.Boolean;
                if(NormalizedFieldType == typeof(DateTime))
                    return UnboundColumnType.DateTime;

                return UnboundColumnType.Object;
            }
        }

        public int? MinWidth { get; set; }

        private int FinalMinWidth
        {
            get { return MinWidth.HasValue ? MinWidth.Value : _gridColumn.MinWidth; }
        }

        public bool IsCalculatedColumn
        {
            get { return ! string.IsNullOrWhiteSpace(UnboundExpression); }
        }

        public string DefaultFormatString
        {
            get { return TypeToDefaultFormatString(NormalizedFieldType); }
        }

        public Type FieldType { get; set; }

        private Type NormalizedFieldType
        {
            get { return FieldType.GetNormalizedType(); }
        }

        public bool AutoWidth { get; set; }

        public FilterPopupMode? FilterPopupMode { get; set; }

        private FilterPopupMode FinalFilterPopupMode
        {
            get { return FilterPopupMode.HasValue ? FilterPopupMode.Value : DefaultFilterPopupMode; }
        }

        public FilterPopupMode DefaultFilterPopupMode
        {
            get
            {
                if(NormalizedFieldType == typeof(DateTime))
                    return DevExpress.XtraGrid.Columns.FilterPopupMode.DateAlt;

                if(NormalizedFieldType.IsNumericType())
                    return DevExpress.XtraGrid.Columns.FilterPopupMode.List;

                return DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            }
        }

        public GridColumn GetGridColumn(int columnIndex)
        {
            _gridColumn.OptionsFilter.AutoFilterCondition = AutoFilterCondition.Contains;
            _gridColumn.VisibleIndex = columnIndex;

            _gridColumn.FieldName = FinalFieldName;
            _gridColumn.Name = FinalColumnName;
            _gridColumn.Caption = FinalCaption;
            _gridColumn.OptionsFilter.FilterPopupMode = FinalFilterPopupMode;
            _gridColumn.DisplayFormat.FormatString = FinalFormatString;
            _gridColumn.MinWidth = FinalMinWidth;

            if(IsCalculatedColumn)
            {
                _gridColumn.UnboundExpression = FinalUnboundExpression;
                _gridColumn.UnboundType = FinalUnboundExpressionType;
            }

            return _gridColumn;
        }

        private static string TypeToDefaultFormatString(Type type)
        {
            if(type == typeof(String))
                return null;

            if(type == typeof(int))
                return null;
            
            if(type == typeof(decimal))
                return "N0";

            if(type == typeof(DateTime))
                return "MM/dd/yy H:mm:ss";

            return null;
        }
    }
}