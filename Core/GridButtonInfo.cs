using System;
using System.Drawing;
using Lux.BusinessIntelligence.Studio.Enums;
using WinFormsDesign;

namespace Lux.BusinessIntelligence.Studio.Core
{
    public class GridButtonInfo<TRecord>
    {
        public string Caption { get; set; }

        public Action<TRecord> OnClick { get; set; }

        public Func<TRecord, bool> IsVisiblePredicate { get; set; }

        public IconType Icon { get; set; }

        public Image Image
        {
            get
            {
                switch(Icon)
                {
                    case IconType.Restart:
                        return Resources.RestartOperation.ToBitmap().Resize(16, 16);
                    case IconType.Cancel:
                        return Resources.CancelOperation.ToBitmap().Resize(16, 16);
                    case IconType.Refresh:
                        return Resources.RestartOperation.ToBitmap().Resize(16, 16);
                    case IconType.MoveUp:
                        return Resources.UpIcon.ToBitmap().Resize(16, 16);
                    case IconType.MoveDown:
                        return Resources.DownIcon.ToBitmap().Resize(16, 16);
                    default:
                        return null;
                }
            }
        }
    }
}