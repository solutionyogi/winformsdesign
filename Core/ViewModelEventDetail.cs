namespace Lux.BusinessIntelligence.Studio.Core
{
    public class ViewModelEventDetail<TViewModel>
    {
        public TViewModel ViewModel { get; set; }

        public ViewModelEvent Event { get; set; }
    }
}