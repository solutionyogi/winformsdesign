using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Lux.BusinessIntelligence.Core.Helpers;
using NLog;

namespace Lux.BusinessIntelligence.Studio.Core
{
    public class ReadOnlyGridViewFormat<TRecord> where TRecord : class
    {
        private const string ButtonActionColumnName = "ButtonActionColumn";

// ReSharper disable StaticFieldInGenericType
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

// ReSharper restore StaticFieldInGenericType

        private readonly List<GridColumnFormatInfo> _columns = new List<GridColumnFormatInfo>();

        private readonly List<GridButtonInfo<TRecord>> _buttons = new List<GridButtonInfo<TRecord>>();

        private readonly List<StyleFormatCondition> _conditions = new List<StyleFormatCondition>();

        public GridView GridView { get; set; }

        public IEnumerable<GridColumnFormatInfo> Columns
        {
            get { return _columns; }
        }

        public IEnumerable<StyleFormatCondition> Conditions
        {
            get { return _conditions; }
        }

        public IEnumerable<GridButtonInfo<TRecord>> Buttons
        {
            get { return _buttons; }
        }

        public GridColumnFormatInfo AddColumn<TProperty>(Expression<Func<TRecord, TProperty>> propertyExpression, GridColumnFormatInfo formatInfo = null)
        {
            //HACK: To correctly parse expression of form x => x.ABC.DEF, we have to use expression which takes
            //the original object and the expression to get the property. 
            //Currently, we are passing NULL to the GetPropertyInfoExtended method as a workaround. 
            //Replace this code once we have a better logic to parse the expression.
            var propertyInfo = ExpressionHelper.GetPropertyInfoExtended(null, propertyExpression);

            formatInfo = formatInfo ?? new GridColumnFormatInfo();

            formatInfo.FieldName = propertyInfo.FullName;
            formatInfo.FieldType = propertyInfo.PropertyInfo.PropertyType;

            _columns.Add(formatInfo);

            return formatInfo;
        }

        public GridColumnFormatInfo AddCalculatedColumn(string expression, Type expressionType, string caption, GridColumnFormatInfo formatInfo = null)
        {
            if(string.IsNullOrWhiteSpace(caption))
                throw new ArgumentNullException("caption");

            if(string.IsNullOrWhiteSpace(expression))
                throw new ArgumentNullException("expression");

            if(expressionType == null)
                throw new ArgumentNullException("expressionType");

            formatInfo = formatInfo ?? new GridColumnFormatInfo();

            formatInfo.FieldType = expressionType;
            formatInfo.UnboundExpression = expression;
            formatInfo.Caption = caption;
            formatInfo.FieldName = caption;

            _columns.Add(formatInfo);

            return formatInfo;
        }

        public void AddExistingColumns()
        {
            foreach(var propertyInfo in typeof(TRecord).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var gridcolumnFormatInfo = new GridColumnFormatInfo
                                           {
                                               FieldName = propertyInfo.Name,
                                               FieldType = propertyInfo.PropertyType,
                                           };
                _columns.Add(gridcolumnFormatInfo);
            }
        }

        public StyleFormatCondition AddBackgroundColorFormatting(string condition, Color colorToApply)
        {
            var styleFormatCondition = new StyleFormatCondition
                                       {
                                           Condition = FormatConditionEnum.Expression,
                                           Expression = condition
                                       };
            styleFormatCondition.Appearance.BackColor = colorToApply;
            styleFormatCondition.Appearance.ForeColor = Color.Black;
            styleFormatCondition.Appearance.Options.UseBackColor = true;
            styleFormatCondition.Appearance.Options.UseForeColor = true;
            _conditions.Add(styleFormatCondition);
            return styleFormatCondition;
        }

        public void AddButton(GridButtonInfo<TRecord> buttonInfo)
        {
            if(buttonInfo == null)
                throw new ArgumentNullException("buttonInfo");

            if(buttonInfo.OnClick == null)
                throw new InvalidOperationException("You must specify OnClick property of ButtonInfo.");

            if(string.IsNullOrWhiteSpace(buttonInfo.Caption))
                throw new InvalidOperationException("You must specify Caption for the ButtonInfo.");

            _buttons.Add(buttonInfo);
        }

        public GridButtonInfo<TRecord> GetButtonInfo(string buttonName)
        {
            return Buttons.Single(x => x.Caption.Equals(buttonName));
        }

        public void ApplyFormat()
        {
            GridView.BeginUpdate();

            //By default, GridView auto generates columns, we need to clear them
            //and only show the ones requested by the user.
            GridView.Columns.Clear();

            SetGridViewDefaults();

            AddButtons();

            AddColumns();

            AddConditions();

            AddEmptyViewMessage();

            GridView.EndUpdate();
        }

        private void SetButtonVisibility(object sender, CustomRowCellEditEventArgs e)
        {
            if(e.Column.Name != ButtonActionColumnName)
                return;

            //We CANNOT use the GridView reference we have stored in the 'GridView' property.
            //This is because in case of master/detail, the DevExpress component actually creates a CLONE
            //of the detail view and uses that. This means that we always have to use the 'sender'
            //to find the actual GridView.
            //Reference: http://www.devexpress.com/Support/Center/Question/Details/A2158
            var gridView = (GridView) sender;

            //This check is needed because a GridView can have a 'Auto Filter' row visible.
            //In that case, e.RepositoryItem will be RepositoryItemTextEdit for that
            //AutoFilter row and we have to skip it for our formatting purpose.
            var buttonEdit = e.RepositoryItem as RepositoryItemButtonEdit;

            if(buttonEdit == null)
                return;

            var buttonEditClone = (RepositoryItemButtonEdit) buttonEdit.Clone();

            buttonEditClone.Assign(e.RepositoryItem);
            buttonEditClone.ButtonClick += (o, args) => HandleButtonClick(gridView, args);
            e.RepositoryItem = buttonEditClone;

            foreach(var button in buttonEditClone.Buttons.Cast<EditorButton>())
            {
                var buttonInfo = GetButtonInfo((string) button.Tag);
                var row = (TRecord) gridView.GetRow(e.RowHandle);
                
                if(row == null)
                {
                    Logger.Error(string.Format("Could not find the Row from GridView for RowHandle: {0}", e.RowHandle));
                    continue;
                }

                button.Visible = buttonInfo.IsVisiblePredicate == null || buttonInfo.IsVisiblePredicate(row);
            }
        }

        private void HandleButtonClick(GridView gridView, ButtonPressedEventArgs args)
        {
            var buttonInfo = GetButtonInfo((string) args.Button.Tag);

            var record = (TRecord) gridView.GetFocusedRow();

            buttonInfo.OnClick(record);
        }

        private void AddEmptyViewMessage()
        {
            GridView.CustomDrawEmptyForeground += HandleEmptyGridView;
        }

        private static void HandleEmptyGridView(object sender, CustomDrawEventArgs e)
        {
            var view = sender as GridView;

            if(view == null || view.RowCount != 0)
                return;

            using(var drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;

                var message = string.Format("No matching records are available.");

                using(var highlightFont = new Font(e.Appearance.Font.FontFamily, 20, e.Appearance.Font.Style))
                {
                    e.Graphics.DrawString(message, highlightFont, SystemBrushes.ControlDark,
                        new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
                }
            }
        }

        private void AddButtons()
        {
            if(_buttons.Count == 0)
                return;

            var buttonEdit = GetRepositoryItemButtonEdit();

            var buttonColumn = GetActionButtonColumn(buttonEdit);

            GridView.Columns.Add(buttonColumn);

            GridView.CustomRowCellEdit += SetButtonVisibility;
        }

        private static GridColumn GetActionButtonColumn(RepositoryItemButtonEdit buttonEdit)
        {
            var column = new GridColumn
                         {
                             Caption = "Action",
                             VisibleIndex = 0,
                             ShowButtonMode = ShowButtonModeEnum.ShowAlways,
                             MinWidth = 100,
                             Name = ButtonActionColumnName,
                             ColumnEdit = buttonEdit
                         };

            return column;
        }

        private RepositoryItemButtonEdit GetRepositoryItemButtonEdit()
        {
            var buttonEdit = new RepositoryItemButtonEdit
                             {
                                 AutoHeight = false,
                                 Name = "ActionButton",
                                 TextEditStyle = TextEditStyles.HideTextEditor
                             };

            //By default, a new ButtonEdit is always created with a single button.
            //We want to keep only the buttons specified by the user.
            buttonEdit.Buttons.Clear();

            foreach(var button in Buttons)
            {
                var editorButton = new EditorButton
                                   {
                                       Kind = ButtonPredefines.Glyph,
                                       Caption = button.Caption,
                                       Enabled = true,
                                       Visible = true,
                                       ImageLocation = ImageLocation.MiddleLeft,
                                       Tag = button.Caption
                                   };

                if(button.Image != null)
                    editorButton.Image = button.Image;

                buttonEdit.Buttons.Add(editorButton);
            }
            return buttonEdit;
        }

        private void AddConditions()
        {
            foreach(var columnFormatInfo in Conditions)
            {
                GridView.FormatConditions.Add(columnFormatInfo);
            }
        }

        private void AddColumns()
        {
            var columnIndex = Buttons.Any() ? 1 : 0;

            foreach(var columnFormatInfo in Columns)
            {
                var gridColumn = columnFormatInfo.GetGridColumn(columnIndex);
                //We have to disable editing on individual columns instead of doing it at the GridView level
                //because if GridView is non editable, the buttons on the grid row will not fire their click event.
                gridColumn.OptionsColumn.AllowEdit = false;
                GridView.Columns.Add(gridColumn);
                columnIndex++;
            }
        }

        private void SetGridViewDefaults()
        {
            GridView.OptionsView.ShowGroupPanel = false;
            GridView.OptionsView.HeaderFilterButtonShowMode = FilterButtonShowMode.Button;
            GridView.OptionsView.ShowAutoFilterRow = true;
            GridView.OptionsFilter.UseNewCustomFilterDialog = true;
            GridView.OptionsBehavior.Editable = true;
            GridView.OptionsSelection.MultiSelect = true;
            GridView.OptionsBehavior.AutoPopulateColumns = false;
        }
    }
}