using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraLayout;
using Lux.BusinessIntelligence.Core.Helpers;
using Lux.BusinessIntelligence.Studio.Helpers;
using NLog;

namespace Lux.BusinessIntelligence.Studio.Core
{
    public static class ExtensionMethods
    {
        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static IEnumerable<Control> GetControls(this LayoutControlGroup layoutControlGroup)
        {
            if(layoutControlGroup == null)
                throw new ArgumentNullException("layoutControlGroup");

            var layoutItems = layoutControlGroup.Items.OfType<LayoutControlItem>();

            foreach(var layoutControlItem in layoutItems)
            {
                yield return layoutControlItem.Control;
            }
        }

        public static void Subscribe<TViewModel, TEvent>(this TViewModel model, TEvent @event, Action action)
        {
            if(ReferenceEquals(model, null))
                throw new ArgumentNullException("model");

            if(ReferenceEquals(@event, null))
                throw new ArgumentNullException("event");

            if(action == null)
                throw new ArgumentNullException("action");

            ViewManager.EventAggregator.GetEvent<ViewModelEventDetail<TViewModel>>().Subscribe(x =>
                                                                                               {
                                                                                                   if(model.Equals(x.ViewModel) && x.Event.Equals(@event))
                                                                                                       action();
                                                                                               });
        }

        public static Image Resize(this Image inputImage, int width, int height)
        {
            if(inputImage == null)
                throw new ArgumentNullException("inputImage");

            var bitmap = new Bitmap(width, height);
            var graphics = Graphics.FromImage(bitmap);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(inputImage, 0, 0, width, height);
            graphics.Dispose();

            try
            {
                return (Image) bitmap.Clone();
            }
            finally
            {
                bitmap.Dispose();
            }
        }

        public static Binding AddDataBinding<TControl, TModel>(this TControl control, Expression<Func<TControl, object>> controlPropertyExpression, TModel model,
                                                               Expression<Func<TModel, object>> modelPropertyExpression) where TControl : Control
            where TModel : class, INotifyPropertyChanged
        {
            if(control == null)
                throw new ArgumentNullException("control");

            if(controlPropertyExpression == null)
                throw new ArgumentNullException("controlPropertyExpression");

            if(ReferenceEquals(model, null))
                throw new ArgumentNullException("model");

            if(modelPropertyExpression == null)
                throw new ArgumentNullException("modelPropertyExpression");

            var controlExpressionDetail = ExpressionDetail.Create(controlPropertyExpression);
            var viewModelExpressionDetail = ExpressionDetail.Create(modelPropertyExpression);

            var binding = new Binding(controlExpressionDetail.Name, model, viewModelExpressionDetail.Name)
                          {
                              DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
                              ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
                          };

            control.DataBindings.Add(binding);

            return binding;
        }

        public static void SetPlacement(this Form windowForm, string placementXml)
        {
            if(windowForm == null)
                throw new ArgumentNullException("windowForm");

            if(string.IsNullOrWhiteSpace(placementXml))
                throw new ArgumentNullException("placementXml");

            WindowPlacementHelper.SetPlacement(windowForm.Handle, placementXml);
        }

        public static string GetPlacement(this Form windowForm)
        {
            if(windowForm == null)
                throw new ArgumentNullException("windowForm");

            return WindowPlacementHelper.GetPlacement(windowForm.Handle);
        }

        public static EditorButton FindEditorButtonByTag<T>(this RepositoryItemButtonEdit buttonEdit, T tagValue)
        {
            if(buttonEdit == null)
                throw new ArgumentNullException("buttonEdit");

            if(ReferenceEquals(tagValue, null))
                throw new ArgumentNullException("tagValue");

            foreach(var button in from button in buttonEdit.Buttons.Cast<EditorButton>()
                where !string.IsNullOrWhiteSpace(button.Tag.ToString())
                let buttonTagValue = DataHelper.ConvertTo<T>(button.Tag)
                where buttonTagValue.Equals(tagValue)
                select button)
            {
                return button;
            }

            throw new InvalidOperationException(string.Format("Could not find EditorButton with Tag {0}", tagValue));
        }
    }
}