using System;
using Lux.BusinessIntelligence.Studio.Abstractions;

namespace Lux.BusinessIntelligence.Studio.Core
{
    public abstract class ViewModelBase : ObservableObject, IViewModel
    {
        private readonly Lazy<ILuxAppService> _luxAppServiceClient = new Lazy<ILuxAppService>(GetClient);

        private bool _isBusy;

        private static ILuxAppService GetClient()
        {
            throw new NotImplementedException();
        }

        public ILuxAppService ServiceMethod
        {
            get { return _luxAppServiceClient.Value; }
        }

        public abstract string Title { get; }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if(value.Equals(_isBusy))
                    return;
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        public string CurrentUser
        {
            get { return "UI"; }
        }

        public abstract void Initialize();
    }
}