﻿using System;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using Lux.BusinessIntelligence.Studio.Abstractions;

namespace Lux.BusinessIntelligence.Studio.Core
{
    public class ViewBase : XtraForm, IView
    {
        public virtual void Initialize()
        {
            throw new NotImplementedException("You must implement Initialize method in your view.");
        }

        public virtual IViewModel ViewModel { get; set; }

        public virtual LayoutControl MainContainer
        {
            get { throw new NotImplementedException("You must implement main container."); }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if(ViewModel == null)
                return;

            Initialize();
        }

        protected TViewModel GetViewModel<TViewModel>()
        {
            if(ViewModel.GetType() != typeof(TViewModel))
            {
                throw new InvalidOperationException(string.Format("Can not convert ViewModel of type {0} into type {1}", ViewModel.GetType().Name,
                    typeof(TViewModel).Name));
            }

            return (TViewModel) ViewModel;
        }
    }
}