using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using Fasterflect;
using Lux.BusinessIntelligence.Studio.Abstractions;
using Lux.BusinessIntelligence.Studio.Enums;
using Microsoft.NetEnterpriseServers;
using Reactive.EventAggregator;

namespace Lux.BusinessIntelligence.Studio.Core
{
    public static class ViewManager
    {
        private static readonly Dictionary<string, ConstructorInfo> ViewTypeConstructorCache = GetViewTypeConstructorCache();

        private static Dictionary<string, ConstructorInfo> GetViewTypeConstructorCache()
        {
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.Implements<IView>()).ToList();

            /*
              
            //TODO: Replace Count with ToCsv method. 
            var invalidViewTypes = types.Where(x => !x.Inherits<ViewBase>()).ToList();

            if (invalidViewTypes.Count != 0)
            throw new InvalidOperationException(
                string.Format(
                "Invalid setup. Found following types which implements {0} but does not inherit from {1}. Invalid Type List: {2}",
                typeof (IView).Name, typeof (ViewBase).Name, invalidViewTypes.Count));*/

            return types.ToDictionary(x => x.Name, x => x.Constructor(), StringComparer.OrdinalIgnoreCase);
        }

// ReSharper disable InconsistentNaming
        private static readonly MainForm _mainForm = new MainForm();

        private static readonly EventAggregator _eventAggregator = new EventAggregator();

// ReSharper restore InconsistentNaming

        public static EventAggregator EventAggregator
        {
            get { return _eventAggregator; }
        }

        public static MainForm MainForm
        {
            get { return _mainForm; }
        }

        public static void ShowLogForm()
        {
            var form = MainForm.GetLogForm(LogFormType.WindowsApplicationLog);
            form.MdiParent = MainForm;
            form.Show();
        }

        public static void Show(IViewModel viewModel)
        {
            if(viewModel == null)
                throw new ArgumentNullException("viewModel");

            viewModel.Initialize();

            var view = GetView(viewModel);

            view.MdiParent = MainForm;
            view.Text = viewModel.Title;
            view.ViewModel = viewModel;
            view.Show();
        }

        public static IView GetView(IViewModel viewModel)
        {
            if(viewModel == null)
                throw new ArgumentNullException("viewModel");

            var viewModelTypeName = viewModel.GetType().Name;

            var viewTypeName = viewModelTypeName.Substring(0, viewModelTypeName.LastIndexOf("Model", StringComparison.OrdinalIgnoreCase));

            return GetViewInstance(viewTypeName, viewModelTypeName);
        }

        public static IView GetViewInstance(string viewTypeName, string viewModelTypeName)
        {
            if(!ViewTypeConstructorCache.ContainsKey(viewTypeName))
                throw new InvalidOperationException(string.Format("Could not find View with name {0} for model {1}", viewTypeName, viewModelTypeName));

            var constructor = ViewTypeConstructorCache[viewTypeName];
            return (IView) constructor.CreateInstance();
        }

        private static void ShowUnhandledError(Exception exception)
        {
            var invalidOperationException = new InvalidOperationException("There was an error processing your request.", exception);
            var exceptionBox = new ExceptionMessageBox(invalidOperationException);
            exceptionBox.Show(MainForm);
        }

        public static IView ActiveView
        {
            get
            {
                var currentDocument = MainForm.DocumentManager.View.ActiveDocument;
                return currentDocument.Form as IView;
            }
        }

        public static void HandleException(Exception exception)
        {
            if(exception == null)
                return;

            //If for some reason, we are dealing with a Form which is not using MVVM pattern
            //we will use an alternate mechanism to display the exception.
            if(ActiveView == null)
            {
                ShowUnhandledError(exception);
                return;
            }

            ShowErrorOnActiveDocument(exception);
        }

        private static void ShowErrorOnActiveDocument(Exception e)
        {
            var errorLayoutGroup = GetErrorLayoutGroup(ActiveView);

            errorLayoutGroup.Clear();

            var errorLabelControl = new LabelControl
                                    {
                                        Text = e.Message
                                    };

            var errorLabelLayoutItem = errorLayoutGroup.AddItem();
            errorLabelLayoutItem.Control = errorLabelControl;
            errorLabelLayoutItem.TextVisible = false;
        }

        private static LayoutControlGroup GetErrorLayoutGroup(IView currentView)
        {
            var mainLayoutControl = currentView.MainContainer;

            const string errorLayoutGroupControlName = "ErrorLayoutGroup";

            var layoutItem = mainLayoutControl.Items.FindByName(errorLayoutGroupControlName);

            var errorLayoutGroup = layoutItem as LayoutControlGroup;

            if(errorLayoutGroup != null)
                return errorLayoutGroup;

            errorLayoutGroup = mainLayoutControl.Root.AddGroup(mainLayoutControl.Root.Items[0], InsertType.Top);

            errorLayoutGroup.Name = errorLayoutGroupControlName;
            errorLayoutGroup.Text = "Error";
            return errorLayoutGroup;
        }
    }
}