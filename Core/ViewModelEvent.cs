namespace Lux.BusinessIntelligence.Studio.Core
{
    public enum ViewModelEvent
    {
        None,

        CalculationInProgress
    }
}