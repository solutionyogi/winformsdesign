﻿using System;
using System.Data;
using System.Linq;
using Lux.BusinessIntelligence.Core.DTO.UI;
using Lux.BusinessIntelligence.Core.ExtensionMethods;

namespace Lux.BusinessIntelligence.Studio
{
    public static class HierarchyDataSetGenerator
    {
        private const string DtdPnlColumnName = "DtdPnlTotal";

        private const string MtdPnlColumnName = "MtdPnlTotal";

        private const string QtdPnlColumnName = "QtdPnlTotal";

        private const string YtdPnlColumnName = "YtdPnlTotal";

        private const string LevelColumnName = "Level";

        public static DataSet Generate()
        {
            var dataset = new DataSet();

            CreateTable("Portfolio");

            var portfolioTable = CreateTable("Portfolio", "PortfolioCode");
            var strategyTable = CreateTable("Strategy", "PortfolioCode", "StrategyCode");
            var positionTable = CreateTable("Position", "PortfolioCode", "StrategyCode", "InvestmentCode");

            dataset.Tables.Add(portfolioTable);
            dataset.Tables.Add(strategyTable);
            dataset.Tables.Add(positionTable);

            dataset.Relations.Add("PortfolioStrategy", GetColumns(portfolioTable, "PortfolioCode"), GetColumns(strategyTable, "PortfolioCode"));
            dataset.Relations.Add("StrategyPosition", GetColumns(strategyTable, "PortfolioCode", "StrategyCode"),
                GetColumns(positionTable, "PortfolioCode", "StrategyCode"));

            GenerateRows(portfolioTable, null, 10, "PortfolioCode");
            GenerateRows(strategyTable, portfolioTable, 4, "StrategyCode");
            GenerateRows(positionTable, strategyTable, 20, "InvestmentCode");

            return dataset;
        }

        private static void GenerateRows(DataTable childTable, DataTable parentTable, int rowCount, string columnName)
        {
            if (parentTable == null)
            {
                for (var index = 0; index < rowCount; index++)
                {
                    var childRow = childTable.NewRow();
                    SetChildRowValues(childRow, columnName, index);
                    childTable.Rows.Add(childRow);
                }

                childTable.AcceptChanges();
            }
            else
            {
                foreach (var parentRow in parentTable.Rows.Cast<DataRow>())
                {
                    for (var index = 0; index < rowCount; index++)
                    {
                        var childRow = childTable.NewRow();
                        SetChildRowValues(childRow, columnName, index);
                        AddParentRowValues(childRow, parentRow);
                        childTable.Rows.Add(childRow);
                    }
                }

                childTable.AcceptChanges();
            }
        }

        private static void SetChildRowValues(DataRow dataRow, string columnName, int index)
        {
            var columnValue = columnName[0].ToString() + index;
            dataRow[columnName] = columnValue;
            dataRow[LevelColumnName] = columnValue;
            AddPnlValues(dataRow);
        }

        private static void AddParentRowValues(DataRow childRow, DataRow parentRow)
        {
            var childTable = childRow.Table;
            var dataset = childTable.DataSet;
            var relation = dataset.Relations.Cast<DataRelation>().Single(x => x.ChildTable == childTable);
            foreach (var childColumn in relation.ChildColumns)
            {
                childRow[childColumn] = parentRow[childColumn.ColumnName];
            }
        }

        private static void AddPnlValues(DataRow row)
        {
            var random = new Random();
            row[DtdPnlColumnName] = random.Next(10000, 100000) + random.NextDouble() * 100;
            row[MtdPnlColumnName] = random.Next(10000, 500000) + random.NextDouble() * 100;
            row[QtdPnlColumnName] = random.Next(10000, 1000000) + random.NextDouble() * 100;
            row[YtdPnlColumnName] = random.Next(10000, 5000000) + random.NextDouble() * 100;
        }

        private static DataColumn[] GetColumns(DataTable table, params string[] columnNames)
        {
            return table.Columns.Cast<DataColumn>().Where(x => x.ColumnName.In(columnNames)).ToArray();
        }

        private static DataTable CreateTable(string tableName, params string[] columnList)
        {
            var table = new DataTable(tableName);

            foreach (var columnName in columnList)
            {
                var dataColumn = new DataColumn(columnName, typeof(string));
                var columnFormatInfo = new ColumnFormatInfo
                                       {
                                           Caption = columnName,
                                           IsVisible = false
                                       };

                dataColumn.SetColumnFormatInfo(columnFormatInfo);

                table.Columns.Add(dataColumn);
            }

            var levelColumnFormatInfo = new ColumnFormatInfo
                                        {
                                            Caption = LevelColumnName,
                                            IsVisible = true
                                        };

            var levelColumn = new DataColumn(LevelColumnName, typeof(string));
            levelColumn.SetColumnFormatInfo(levelColumnFormatInfo);
            table.Columns.Add(levelColumn);

            AddPnlColumns(table);

            return table;
        }

        private static void AddPnlColumns(DataTable inputTable)
        {
            inputTable.Columns.Add(GetPnlColumn(DtdPnlColumnName, "Day P&L"));
            inputTable.Columns.Add(GetPnlColumn(MtdPnlColumnName, "MTD P&L"));
            inputTable.Columns.Add(GetPnlColumn(QtdPnlColumnName, "QTD P&L"));
            inputTable.Columns.Add(GetPnlColumn(YtdPnlColumnName, "YTD P&L"));
        }

        private static DataColumn GetPnlColumn(string columnName, string caption)
        {
            var formatInfo = new ColumnFormatInfo
                             {
                                 Caption = caption,
                                 IsVisible = true,
                                 FormatString = "N0"
                             };

            var column = new DataColumn(columnName, typeof(decimal));
            column.SetColumnFormatInfo(formatInfo);
            return column;
        }
    }
}