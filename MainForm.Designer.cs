﻿namespace Lux.BusinessIntelligence.Studio
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MainBarManager = new DevExpress.XtraBars.BarManager(this.components);
            this.MainMenuBar = new DevExpress.XtraBars.Bar();
            this.ReportsSubItem = new DevExpress.XtraBars.BarSubItem();
            this.ReportViewerButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.ReportDesignerButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.GridViewFormatButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.ReadOnlyGridViewButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.HierarchicalGridViewButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.MainDocumentManager = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.MainTabView = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.TestForm1ButtonItem = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainBarManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainDocumentManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainTabView)).BeginInit();
            this.SuspendLayout();
            // 
            // MainBarManager
            // 
            this.MainBarManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.MainMenuBar});
            this.MainBarManager.DockControls.Add(this.barDockControlTop);
            this.MainBarManager.DockControls.Add(this.barDockControlBottom);
            this.MainBarManager.DockControls.Add(this.barDockControlLeft);
            this.MainBarManager.DockControls.Add(this.barDockControlRight);
            this.MainBarManager.Form = this;
            this.MainBarManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ReportsSubItem,
            this.ReportViewerButtonItem,
            this.ReportDesignerButtonItem,
            this.GridViewFormatButtonItem,
            this.ReadOnlyGridViewButtonItem,
            this.HierarchicalGridViewButtonItem,
            this.TestForm1ButtonItem});
            this.MainBarManager.MainMenu = this.MainMenuBar;
            this.MainBarManager.MaxItemId = 7;
            // 
            // MainMenuBar
            // 
            this.MainMenuBar.BarName = "Main menu";
            this.MainMenuBar.DockCol = 0;
            this.MainMenuBar.DockRow = 0;
            this.MainMenuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.MainMenuBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ReportsSubItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.GridViewFormatButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.ReadOnlyGridViewButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.HierarchicalGridViewButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.TestForm1ButtonItem)});
            this.MainMenuBar.OptionsBar.MultiLine = true;
            this.MainMenuBar.OptionsBar.UseWholeRow = true;
            this.MainMenuBar.Text = "Main menu";
            // 
            // ReportsSubItem
            // 
            this.ReportsSubItem.Caption = "Reports";
            this.ReportsSubItem.Id = 0;
            this.ReportsSubItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ReportViewerButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.ReportDesignerButtonItem)});
            this.ReportsSubItem.Name = "ReportsSubItem";
            // 
            // ReportViewerButtonItem
            // 
            this.ReportViewerButtonItem.Caption = "Report Viewer";
            this.ReportViewerButtonItem.Id = 1;
            this.ReportViewerButtonItem.Name = "ReportViewerButtonItem";
            this.ReportViewerButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ReportViewerButtonItemItemClick);
            // 
            // ReportDesignerButtonItem
            // 
            this.ReportDesignerButtonItem.Caption = "Report Designer";
            this.ReportDesignerButtonItem.Id = 2;
            this.ReportDesignerButtonItem.Name = "ReportDesignerButtonItem";
            // 
            // GridViewFormatButtonItem
            // 
            this.GridViewFormatButtonItem.Caption = "Designer Form";
            this.GridViewFormatButtonItem.Id = 3;
            this.GridViewFormatButtonItem.Name = "GridViewFormatButtonItem";
            this.GridViewFormatButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.GridViewFormatButtonItemItemClick);
            // 
            // ReadOnlyGridViewButtonItem
            // 
            this.ReadOnlyGridViewButtonItem.Caption = "Read Only Grid View";
            this.ReadOnlyGridViewButtonItem.Id = 4;
            this.ReadOnlyGridViewButtonItem.Name = "ReadOnlyGridViewButtonItem";
            this.ReadOnlyGridViewButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarButtonItem1ItemClick);
            // 
            // HierarchicalGridViewButtonItem
            // 
            this.HierarchicalGridViewButtonItem.Caption = "Hierarhical Grid View";
            this.HierarchicalGridViewButtonItem.Id = 5;
            this.HierarchicalGridViewButtonItem.Name = "HierarchicalGridViewButtonItem";
            this.HierarchicalGridViewButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.HierarchicalGridViewButtonItemItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(719, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 584);
            this.barDockControlBottom.Size = new System.Drawing.Size(719, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 562);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(719, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 562);
            // 
            // MainDocumentManager
            // 
            this.MainDocumentManager.MdiParent = this;
            this.MainDocumentManager.MenuManager = this.MainBarManager;
            this.MainDocumentManager.View = this.MainTabView;
            this.MainDocumentManager.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.MainTabView});
            // 
            // TestForm1ButtonItem
            // 
            this.TestForm1ButtonItem.Caption = "Test Form1";
            this.TestForm1ButtonItem.Id = 6;
            this.TestForm1ButtonItem.Name = "TestForm1ButtonItem";
            this.TestForm1ButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.TestForm1ButtonItemItemClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 584);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.Text = "Main Form";
            ((System.ComponentModel.ISupportInitialize)(this.MainBarManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainDocumentManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainTabView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager MainBarManager;
        private DevExpress.XtraBars.Bar MainMenuBar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem ReportsSubItem;
        private DevExpress.XtraBars.BarButtonItem ReportViewerButtonItem;
        private DevExpress.XtraBars.BarButtonItem ReportDesignerButtonItem;
        private DevExpress.XtraBars.Docking2010.DocumentManager MainDocumentManager;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView MainTabView;
        private DevExpress.XtraBars.BarButtonItem GridViewFormatButtonItem;
        private DevExpress.XtraBars.BarButtonItem ReadOnlyGridViewButtonItem;
        private DevExpress.XtraBars.BarButtonItem HierarchicalGridViewButtonItem;
        private DevExpress.XtraBars.BarButtonItem TestForm1ButtonItem;
    }
}

