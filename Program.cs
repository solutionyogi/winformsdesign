﻿using System;
using System.Threading;
using System.Windows.Forms;
using Lux.BusinessIntelligence.Studio.Core;

namespace Lux.BusinessIntelligence.Studio
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += UnhandledThreadException;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += UnhandledAppDomainException;

            Application.Run(ViewManager.MainForm);
        }

        private static void UnhandledAppDomainException(object sender, UnhandledExceptionEventArgs e)
        {
            ViewManager.HandleException((Exception) e.ExceptionObject);
        }

        private static void UnhandledThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ViewManager.HandleException(e.Exception);
        }
    }
}