﻿using System;
using System.ComponentModel;

namespace Lux.BusinessIntelligence.Studio.DTO
{
    internal static class TestDataGenerator
    {
        public static BindingList<OperationInfo> GetSampleData()
        {
            var result = new BindingList<OperationInfo>();

            var random = new Random();

            for(var index = 0; index < random.Next(1, 15); index++)
            {
                result.Add(new OperationInfo
                           {
                               Description = "Operation " + index,
                               OperationInfoId = index,
                               ModifiedBy =
                                   random.Next(0, 6) % 6 == 0 ? "Very Long fake data test name. I need more characters but I dont know what else to" : "Jd",
                               ModifiedOn = DateTime.Now
                           });
            }

            return result;
        }

        public static BindingList<ReportInfo> GetReportInfoList()
        {
            var reportList = new BindingList<ReportInfo>
                             {
                                 new ReportInfo
                                 {
                                     ReportInfoId = 1,
                                     ReportName = "Report A"
                                 },
                                 new ReportInfo
                                 {
                                     ReportInfoId = 2,
                                     ReportName = "Report B"
                                 },
                                 new ReportInfo
                                 {
                                     ReportInfoId = 3,
                                     ReportName = "Report C"
                                 }
                             };
            return reportList;
        }
    }
}