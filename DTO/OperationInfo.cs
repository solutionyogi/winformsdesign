using System;

namespace Lux.BusinessIntelligence.Studio.DTO
{
    public class OperationInfo
    {
        public int OperationInfoId { get; set; }

        public string Description { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}