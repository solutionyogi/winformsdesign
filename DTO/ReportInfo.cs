namespace Lux.BusinessIntelligence.Studio.DTO
{
    public class ReportInfo
    {
        public long ReportInfoId { get; set; }

        public string ReportName { get; set; }
    }
}