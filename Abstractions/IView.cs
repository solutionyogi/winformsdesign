using System.Windows.Forms;
using DevExpress.XtraLayout;

namespace Lux.BusinessIntelligence.Studio.Abstractions
{
    public interface IView
    {
        /// <summary>
        /// Show method of Form class.
        /// </summary>
        void Show();

        /// <summary>
        /// Property which is part of Form class. This allows us to create views inside
        /// our document manager.
        /// </summary>
        Form MdiParent { get; set; }

        /// <summary>
        /// Property to set the Title of the Form. We have to call it Text because
        /// that's how it's named by the Form class.
        /// </summary>
        string Text { get; set; }

        /// <summary>
        /// This method should set up the UI based on the ViewModel property.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Corresponding ViewModel.
        /// </summary>
        IViewModel ViewModel { get; set; }

        LayoutControl MainContainer { get; }
    }
}