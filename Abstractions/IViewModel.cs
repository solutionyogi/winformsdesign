using System;
using System.ComponentModel;

namespace Lux.BusinessIntelligence.Studio.Abstractions
{
    public delegate void EventHandler<in TSender, in TEventArgs>(TSender sender, TEventArgs eventArgs) where TEventArgs : EventArgs;

    public interface IViewModel : INotifyPropertyChanged
    {
        string Title { get; }

        bool IsBusy { get; }

        void Initialize();
    }
}