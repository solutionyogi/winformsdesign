﻿using System;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010;
using Lux.BusinessIntelligence.Studio.Core;
using Lux.BusinessIntelligence.Studio.Views;

namespace Lux.BusinessIntelligence.Studio
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ReportViewerButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            var model = new ReportViewerViewModel();
            ViewManager.Show(model);
        }

        public DocumentManager DocumentManager
        {
            get { return MainDocumentManager; }
        }

        private void GridViewFormatButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            var model = new DesignerViewModel();
            ViewManager.Show(model);
        }

        private void BarButtonItem1ItemClick(object sender, ItemClickEventArgs e)
        {
            ViewManager.Show(new ReadOnlyGridViewModeld());
        }

        public Form GetLogForm(object windowsApplicationLog)
        {
            throw new NotImplementedException();
        }

        private void HierarchicalGridViewButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            ViewManager.Show(new HierarchicalReportViewModel());
        }

        private void TestForm1ButtonItemItemClick(object sender, ItemClickEventArgs e)
        {
            var form = new TestForm1();
            form.MdiParent = this;
            form.Show();
        }
    }
}