﻿namespace Lux.BusinessIntelligence.Studio.Enums
{
    public enum IconType
    {
        Restart,

        Cancel,

        Refresh,

        MoveUp,

        MoveDown,
    }
}