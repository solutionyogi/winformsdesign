namespace Lux.BusinessIntelligence.Studio.Enums
{
    public enum LogFormType
    {
        WindowsApplicationLog,

        ServerApplicationLog
    }
}