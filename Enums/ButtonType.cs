﻿namespace Lux.BusinessIntelligence.Studio.Enums
{
    public enum ButtonType
    {
        RestartButton,

        CancelButton,

        RefreshButton,

        MoveUpButton,

        MoveDownButton,
    }
}